function arrayToObject(arr) {
    var indeksBio=[];
    var nameBio;
    var age;
    urut=1;
    var a = arr.length;
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)           
    for(var indeks = 0; indeks<a;indeks++){
        Bio=[];
        if(arr[indeks][3]>0 && arr[indeks][3]<thisYear){
            age=thisYear-arr[indeks][3]
        }else{
            age = "Invalid Birth Year"
        }
      Bio.firstname=arr[indeks][0]
      Bio.lastname=arr[indeks][1]
      Bio.gender=arr[indeks][2]
      Bio.age=age;
      nameBio=urut+ ". " +Bio.firstname+" "+Bio.lastname
      indeksBio[nameBio]=Bio;
      urut++;
    }
    console.log(indeksBio)
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
console.log("Soal No. 1")
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
console.log("---------------------------------")
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""
console.log("---------------------------------")
console.log("Soal No. 2")

function shoppingTime(memberId = 0, money) {
    var member=[]
    var list=[]
    if(memberId==0){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }else if(money<50000){
        return "Mohon maaf, uang tidak cukup"
    }else{
        member.memberId=memberId;
        member.money=money

        if(money>=1500000){
            list.push("Sepatu Stacattu")
            money=money-1500000;
        }

        if(money>=500000){
            list.push("Baju Zoro")
            money=money-500000;
        }

        if(money>=250000){
            list.push("Baju H&N")
            money=money-250000;
        }

        if(money>=175000){
            list.push("Sweater Uniklooh")
            money=money-175000;
        }

        if(money>=50000){
            list.push("Casing Handphone")
            money=money-50000;
        }
        member.listPurchased=list
        member.changeMoney=money;
        return member
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("---------------------------------")
console.log("Soal No. 3")


  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    indeks=0;
    var b =rute.length;
    all = []
    for(var a = arrPenumpang.length;a>0;a--){
        list=[];
        list.penumpang=arrPenumpang[indeks][0] 
        list.naikDari=arrPenumpang[indeks][1] 
        list.tujuan=arrPenumpang[indeks][2] 
        
        var start=arrPenumpang[indeks][1];
        var finish=arrPenumpang[indeks][2];
        var mulai=0;
        var bayar = 0;
        for(var c=0;c<b;c++){
            if(start==rute[c])
            {  
              mulai=1;
            }

            if(mulai==1 && start!=rute[c]){
                bayar+=2000;
            }

            if (finish==rute[c]){
                mulai=0;
            }
            
        }
        list.bayar=bayar;
        all[indeks]=list;
        indeks++;
    }
    return all
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]