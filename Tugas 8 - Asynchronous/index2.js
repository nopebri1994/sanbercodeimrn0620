var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 

// Lanjutkan code untuk menjalankan function readBooksPromise 


var time = 10000
 function recurse(a){
    let nextBook = a + 1;
    if(nextBook<=books.length && time>0){
            readBooksPromise(time,books[a])
                .then(function (fulfilled) {
                    time=fulfilled;
                    recurse(nextBook);
                })
                .catch(function (error) {
                    console.log(error)
                })
    }
 }

 recurse(0);