console.log('Soal No. 1');
console.log('Release 01')
class Animal {
    constructor(name){
        this._name=name;
        this._legs=4;
        this._cold_blooded=false;
    }
    get name(){
        return this._name;
    }
    set name(a){
        this._name=a;
    }
    get legs(){
        return this._legs;
    }
    set legs(b){
        this._legs=b;
    }
    get cold_blooded(){
        return this._cold_blooded;
    }
    set cold_blooded(c){
        this._cold_blooded=c;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log('--------------------------------')
console.log('Release 02')

// Code class Ape dan class Frog di sini

class Ape extends Animal{
    constructor(name, yell){
        super(name);
        this._yell = "Auooo"
    }
    get yell(){
        return this._yell;
    }
    set yell(a){
        this._yell=a;
    }
    yell() {
        console.log(this._yell)
    }
}

class Frog extends Animal{
    constructor(name, yell){
        super(name);
        this._jump = "hop hop"
    }
    get jump(){
        return this._jump;
    }
    set jump(a){
        this._jump=a;
    }
    jump() {
        console.log(this._jump)
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log('--------------------------------')
console.log('Soal No 02')


class Clock {
    constructor({template}){
        this.template=template
    }

    render(){
        var date = new Date();
  
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
            console.log(output)
    }
    stop() {
        clearInterval(this.timer);
      };
    
    start(){
       this.render();
       this.timer = setInterval(()=>this.render(), 1000);
      };

}

var clock = new Clock({template: 'h:m:s'});
clock.start();  
//clock.stop();

/*
function Clock({ template }) {
  
    var timer;
  
    function render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    this.stop = function() {
      clearInterval(timer);
    };
  
    this.start = function() {
      render();
      timer = setInterval(render, 1000);
    };
  
  }
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 
  */