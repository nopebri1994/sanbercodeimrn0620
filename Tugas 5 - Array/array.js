console.log('Soal no. 1')

function range(a=0,b=0){ 
if(a==0 || a>0 && b==0){
    return -1;
}else{
        var array=[];
        if(b<a){
            while(b <= a){
                array.push(a);
                a--; 
                }
        }else{
            while(a <= b){
            array.push(a);
            a++; 
            }
        }
        return array;   
    }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log('-------------------------')
console.log('Soal no. 2')

function rangeWithStep(a,b,c){
    var array=[];
    var array=[];
    if(b<a){
        while(b <= a){
            array.push(a);
            a-=c; 
            }
    }else{
        while(a <= b){
        array.push(a);
        a+=c; 
        }
    }
    return array;   
}
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log('-------------------------')
console.log('Soal no. 3')

function sum(a,b=0,c=1){
    var array=[];
    var jumlah=0;
    if(b<a){
        while(b <= a){
            array.push(a);
            jumlah+=a;
            a-=c; 
            
            }
    }else{
        while(a <= b){
        array.push(a);
        jumlah+=a;
        a+=c; 
        }
    }
    return jumlah;   
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log('-------------------------')
console.log('Soal no. 4')

function dataHandling(a){
    var end=a.length;
    while(end>=1){
        satu=a[a.length -end];

        console.log("Nomor ID : "+satu[0]);
        console.log("Nama Lengkap : "+satu[1]);
        console.log("TTL : "+satu[2]+", "+satu[3]);
        console.log("Hobby : "+satu[4]);
        console.log('');
        end--;
    }
}

//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

dataHandling(input);

console.log('-------------------------')
console.log('Soal no. 5')

function balikKata(a){
    var end=1;
    r_kata="";
    while(end<=a.length){
        r_kata+=a[a.length -end];
        end++;
    }
    return r_kata;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log('-------------------------')
console.log('Soal no. 6')
function dataHandling2(a){
    nama_awal=a[1];
    a.splice(1, 1, "Roman Alamsyah Elsharawy")
    a.splice(2, 1, "Provinsi Bandar Lampung")
    a.splice(4,1, "Pria")
    a.push("SMA Internasional Metro")
    console.log(a);

    var irish=a.slice(3,4);
    var splitt=irish[0].split("/");
    bulan=splitt[1];
    tahun=splitt[2];
    tanggal=splitt[0];
    namaBulan="";
    switch(bulan) {
        case '01':   { namaBulan='Januari'; break; }
        case '02':   { namaBulan='Februari'; break; }
        case '03':   { namaBulan='Maret'; break; }
        case '04':   { namaBulan='April'; break; }
        case '05':   { namaBulan='Mei'; break; }
        case '06':   { namaBulan='Juni'; break; }
        case '07':   { namaBulan='Juli'; break; }
        case '08':   { namaBulan='Agustus'; break; }
        case '09':   { namaBulan='September'; break; }
        case '10':  { namaBulan='Oktober'; break; }
        case '11':  { namaBulan='Nopember'; break; }
        case '12':  { namaBulan='Desember';break; }
        default:  { b=0; console.log('bulan harus diisi antara 1 - 12'); }}
    console.log(namaBulan);
    splitt.pop();
    splitt.unshift(tahun);
    console.log(splitt);
    console.log(tanggal+"-"+bulan+"-"+tahun);
    console.log(nama_awal);
    

}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 