import React from 'react';
import {View,Text,StyleSheet,Image, TouchableOpacity, FlatList, TextInput,ScrollView} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export default class App extends React.Component{

  render(){
    return(
      <ScrollView style={styles.container}>
        <View style={styles.header}>
          <Image source={require('./asset/logo.png')} />
        </View>
        <View style={styles.logoText}>
          <Text style={styles.text}>
            Login
          </Text>
        </View>
        <View style={styles.inputReg}>
          <Text>
            Username / Email
          </Text>
            <TextInput
              style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
            />
             <Text style={{paddingTop:16}}>
            Password
          </Text>
          <TextInput
              style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
              secureTextEntry={true}
            />
        </View>
        <TouchableOpacity>
        <View style={styles.buttonLog}>
              <Text style={styles.buttonMas}>Masuk</Text>
        </View>
        </TouchableOpacity>
        <TouchableOpacity>
        <View style={styles.TextPilihan}>
              <Text style={styles.pilihanText}>atau</Text>
        </View>
        </TouchableOpacity>
        <TouchableOpacity>
        <View style={styles.buttonLogg}>
          <Text style={styles.button}>Daftar ?</Text>
        </View>
        </TouchableOpacity>
       </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1
  },
  header:{
    paddingTop:63,
    alignItems:"center",
  },
  logoText:{
    paddingTop:88,
    alignItems:"center",
  },
  text:{
   fontSize:25,
  },
  inputReg:{
    paddingTop:40,
    paddingLeft:41,
    paddingRight:41,
    paddingHorizontal: 15,  
  },
  buttonLog:{
    paddingTop:40,
    alignItems:"center"
  },
  button:{
    backgroundColor: '#003366',
    width:140,
    height:40, 
    color:'white',
    fontSize:24,
    textAlign:"center",
    textAlignVertical:"center",
    borderRadius:16,
  },
  TextPilihan:{
    paddingTop:16,
    paddingLeft:163,
    paddingRight:155,
  },
  pilihanText:{
    color:'#3EC6FF',
    textAlign:"center",
    textAlignVertical:"center",
    fontSize:24,
  },
  buttonMas:{
    backgroundColor: '#3EC6FF',
    width:140,
    height:40, 
    color:'white',
    fontSize:24,
    textAlign:"center",
    textAlignVertical:"center",
    borderRadius:16,
  },
  buttonLogg:{
    paddingTop:16,
    paddingBottom:40,
    alignItems:"center"
  },
})