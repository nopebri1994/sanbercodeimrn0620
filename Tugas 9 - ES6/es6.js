console.log('Soal No. 1')

golden = () => {
    return console.log("this is golden!!")  
}
golden();

console.log('----------------------------')
console.log('Soal No. 2')

 const newFunction = function literal(firstName, lastName){
    return {
      firstName,
      lastName,
      fullName: function(){
        console.log(`${firstName} ${lastName}`)
        return 
      }
    }
  }

  newFunction("William", "Imoh").fullName() 

console.log('----------------------------')
console.log('Soal No. 3')


const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
  let {firstName, lastName, third, destination, occupation} = newObject;

console.log(firstName, lastName, destination, occupation)

console.log('----------------------------')
console.log('Soal No. 4')

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
//Driver Code
console.log(combined)

console.log('----------------------------')
console.log('Soal No. 5')

const planet = "earth"
const view = "glass"
const string1="Lorem"
const string2="dolor sit amet consectetur adipiscing elit,"
const string3="do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam"
const after = `${string1} ${view} ${string2} ${planet} ${string3}`
// Driver Code
console.log(after) 