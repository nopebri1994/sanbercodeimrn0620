import React from 'react';
import {View,Text,StyleSheet,Image, TouchableOpacity, FlatList, TextInput,ScrollView} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import { AntDesign } from '@expo/vector-icons';
import { Fontisto } from '@expo/vector-icons'; 
import { Entypo } from '@expo/vector-icons'; 

export const Profile = ({ navigation }) => {
    return(
      <ScrollView style={styles.container}>
       <View style={styles.header}>
           <Text style={{color:'#003366',fontSize:36,fontWeight:'bold',textAlign:"center"}}>Tentang Saya</Text>
       </View>
       <View style={{paddingTop:12,alignItems:"center"}}>
                <Image source={require('./asset/profile.png')} style={styles.image} />
       </View>
       <View style={{paddingTop:24}}>
           <Text style={{color:'#003366',fontSize:24,fontWeight:'bold',textAlign:"center"}}>Nopebri Ade Candra</Text>
       </View>
       <View style={{paddingTop:8}}>
           <Text style={{color:'#3EC6FF',fontSize:16,fontWeight:'bold',textAlign:"center"}}>React Native Developer</Text>
       </View>
       <View style={{alignItems:"center", paddingTop:16}}>
            <View style={styles.box}>
                <Text style={{fontSize:18,color:'#003366',padding:5}}>
                    Portofolio
                </Text>
                <View style={{alignItems:"center"}}>
                     <View style={{width:330,height:1,backgroundColor:'#003366',border:1}} />
                </View>
                <View style={{alignItems:"center", paddingTop:18,flexDirection:"row",justifyContent:"space-around"}}>
                    <Fontisto name="gitlab" size={53} color="skyblue" />
                    <AntDesign name="github" size={53} color="skyblue" />
                </View>
                <View style={{alignItems:"center",paddingTop:8,flexDirection:"row",justifyContent:"space-around"}}>
                <Text style={{fontSize:18,color:'#003366'}}>
                    @nopebri1994
                </Text>
                <Text style={{fontSize:18,color:'#003366'}}>
                    @nopebri1994
                </Text>
                </View>
           </View>
        </View>
        <View style={{alignItems:"center", paddingTop:16,paddingBottom:16   }}>
            <View style={styles.boxSosial}>
                <Text style={{fontSize:18,color:'#003366',padding:5}}>
                    Hubungi Saya
                </Text>
                <View style={{alignItems:"center"}}>
                     <View style={{width:330,height:1,backgroundColor:'#003366',border:1}} />
                </View>
                <View style={styles.sosial}>
                    <Entypo name="facebook" size={53} color="skyblue" />
                    <Text style={{fontSize:18,color:'#003366',fontWeight:"bold"}}>
                    @n0pebri
                    </Text>
                </View>
                <View style={styles.sosial}>
                <AntDesign name="instagram" size={53} color="skyblue" />
                    <Text style={{fontSize:18,color:'#003366',fontWeight:"bold"}}>
                    @nopebri
                    </Text>
                </View>
                <View style={styles.sosial}>
                <AntDesign name="twitter" size={53} color="skyblue" />
                    <Text style={{fontSize:18,color:'#003366',fontWeight:"bold"}}>
                    @nopebri94
                    </Text>
                </View>
           </View>
           
        </View>
       </ScrollView>
    )
  }

const styles = StyleSheet.create({
  container:{
    flex:1,
  },
  header:{
      paddingTop:64,
      paddingLeft:78,
      paddingRight:77,
  },
  image:{
      borderRadius:100,
      width:200,
      height:200,
      alignItems:"center",
  },
  box:{
      backgroundColor:'#EFEFEF',
      borderRadius:16,
     height:140,
     width:340,
  },
  boxSosial:{
    backgroundColor:'#EFEFEF',
    borderRadius:16,
   height:251,
   width:340,
},
sosial:{
    alignItems:"center",
    paddingTop:18,
    paddingLeft:80,
    paddingRight:80,
    flexDirection:"row",
    justifyContent:"space-evenly"
}
})