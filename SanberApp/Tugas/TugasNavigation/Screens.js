import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";
import LoginPage from './Login';
import RegisterPage from './Register';
import SkillPage from './skilscreen';
import AboutPage from './About';
import ProjectPage from './ProjectScreen';
import AddPage from './AddScreen';
//import { AuthContext } from "./context";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 10,
    borderRadius: 5
  },
  title:{
    textAlign:"center",
  }
});

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Skill = ({ navigation }) => (
  <ScreenContainer>
    <SkillPage />
  </ScreenContainer>
);

export const Details = ({route}) => (
  <ScreenContainer>
    <Text>Details Screen</Text>
{route.params.name &&<Text>{route.params.name}</Text>}
  </ScreenContainer>
);

export const Project = ({ navigation }) => (
  <ScreenContainer>
    <ProjectPage />
  </ScreenContainer>
);

export const Add = ({ navigation }) => (
  <ScreenContainer>
    <AddPage />
  </ScreenContainer>
);

export const Search2 = () => (
  <ScreenContainer>
    <Text>Search2 Screen</Text>
  </ScreenContainer>
);

export const Profile = ({ navigation }) => {
  return (
    <ScreenContainer>
       <AboutPage />
    </ScreenContainer>
  );
};
export const Login = ({ navigation }) => {
  return (
    <ScreenContainer>
       < LoginPage />
    </ScreenContainer>
  );
};

export const CreateAccount = () => {
  return (
    <ScreenContainer>
      <RegisterPage />
    </ScreenContainer>
  );
};

