import React from "react";
import { StyleSheet, Text, View } from "react-native";
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Project, Skill, Add} from './Screens';
import {SignIn} from './Login';
import {CreateAccount} from './Register';
import {Profile} from './About';
import {createBottomNavigator, createBottomTabNavigator} from '@react-navigation/bottom-tabs'; 
import {createDrawerNavigator} from '@react-navigation/drawer';
import { Ionicons } from '@expo/vector-icons';

const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();
const ProfileStack = createStackNavigator();

const HomeStackScreen = () =>(
  <HomeStack.Navigator>
    <HomeStack.Screen name="Skill" component={Skill} />
  </HomeStack.Navigator>
)

const ProjectStackScreen = () =>(
  <SearchStack.Navigator>
     <SearchStack.Screen name="Project Screen" component={Project} />
  </SearchStack.Navigator>
)

const AddStackScreen = () =>(
  <SearchStack.Navigator>
    <SearchStack.Screen name="Add Screen" component={Add} />
  </SearchStack.Navigator>
)
const DrawerStack = createDrawerNavigator();

const Tab = () =>(
       <Tabs.Navigator>
             <Tabs.Screen name="Skil Screen" component={HomeStackScreen} />
            <Tabs.Screen name="Project Screen" component={ProjectStackScreen} />
            <Tabs.Screen name="Add Screen" component={AddStackScreen} />
        </Tabs.Navigator>        
);

const Drawer = () => (
       <DrawerStack.Navigator>
         <DrawerStack.Screen name="Profile" component={Profile} />
          <DrawerStack.Screen  name="Skill" component={Tab} />
       </DrawerStack.Navigator>
)

export default function App() {
  return (
     <NavigationContainer >
         <AuthStack.Navigator 
         initialRouteName="Home"
         headerMode="screen"
         screenOptions={{
           headerTintColor: 'white',
           headerStyle: { backgroundColor: 'skyblue' },
           headerTitleAlign:"center"
         }}>
            <AuthStack.Screen name="SignIn" component={SignIn} options={{title: 'Login Your Account'}} />
            <AuthStack.Screen name="CreateAccount" component={CreateAccount} options={{title: 'Create Account'}} />
            <AuthStack.Screen name="Profile" component={Drawer} options={{title: 'About Me',
          }} />
         </AuthStack.Navigator>
     </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
