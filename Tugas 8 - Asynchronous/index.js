// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]


var time = 10000
 function recurse(a){
    let nextBook = a + 1;
    readBooks(time, books[a], function(sisa){ 
        if(nextBook<books.length && sisa>0){
            time=sisa;
            recurse(nextBook);
        }
    })
 }

 recurse(0);